zip -r testproxy.zip apiproxy

access_token=$(gcloud auth print-access-token)

echo $(curl -i -X POST \
   -H "Content-Type:multipart/form-data" \
   -H "Authorization:Bearer $access_token" \
   -F "file=@\"./testproxy.zip\";type=application/zip;filename=\"testproxy.zip\"" \
 'https://apigee.googleapis.com/v1/organizations/bruno-1407a/apis?name=test1&action=import')